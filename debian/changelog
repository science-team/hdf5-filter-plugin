hdf5-filter-plugin (0.0~git20221111.49e3b65-4) unstable; urgency=medium

  * Removed mention of bitshuffle from package description
  * Provides: hdf5-filter-plugin-bz2-serial, hdf5-filter-plugin-lz4-serial
    to follow https://salsa.debian.org/science-team/h5f-packaging-guidelines
  * Added simple autopkgtests
  * Moved project to science-team group, updated Vcs-* headers accordingly

 -- Enrico Zini <enrico@debian.org>  Sun, 29 Jan 2023 11:56:33 +0100

hdf5-filter-plugin (0.0~git20221111.49e3b65-3) unstable; urgency=medium

  [ Enrico Zini ]
  * Disable building the bitshuffle plugin (Closes: #1025391)
  * Added patch to link against hdf5 (Closes: #1025686)

  [ Thorsten Alteholz ]
  * debian/control: Maintainer: set to Freexian, moved myself to Uploaders:

 -- Thorsten Alteholz <debian@alteholz.de>  Sat, 17 Dec 2022 17:07:18 +0100

hdf5-filter-plugin (0.0~git20221111.49e3b65-2) unstable; urgency=medium

  * adapt install path of plugin (Closes: #1024804)

 -- Thorsten Alteholz <debian@alteholz.de>  Mon, 28 Nov 2022 19:07:18 +0100

hdf5-filter-plugin (0.0~git20221111.49e3b65-1) unstable; urgency=medium

  * New upstream release.
    (all patches applied upstream)

 -- Thorsten Alteholz <debian@alteholz.de>  Fri, 18 Nov 2022 18:17:18 +0000

hdf5-filter-plugin (0.0~git20210706.d469f17-1) unstable; urgency=medium

  * New upstream release.
  * add patch to make tests work
    (thanks to Enirco Zini)

 -- Thorsten Alteholz <debian@alteholz.de>  Wed, 02 Nov 2022 22:54:31 +0000
